Quassel is a program to connect to an IRC network. It has the unique ability to split the graphical component (**quasselclient**) from the part that handles the IRC connection (**quasselcore**).
This means that you can have a remote core permanently connected to one or more IRC networks and attach a client from wherever you are without moving around any information or settings.

This cloud app is the **quasselcore** server component.

Desktop chat clients can be downloaded [here](http://www.quassel-irc.org/downloads). Third-party mobile clients for Android and iOS also exist.

#!/bin/bash

set -eux

# Quassel requires pem and key in a single file
SSL_CERT="/app/data/quasselCert.pem"

if [[ -z "$(ls -A /app/data/)" ]]; then
    cp /app/code/quasselcore.conf /app/data/
    cp /app/code/quassel-storage.sqlite /app/data/

    openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout "${SSL_CERT}" -out "${SSL_CERT}" -batch
    fingerprint=`cat "${SSL_CERT}" | openssl x509 -fingerprint | head -1 | sed "s/SHA1 Fingerprint=//"`
    sed -i "s/##SSL_FINGERPRINT/${fingerprint}/" /app/code/welcome/index.html
fi

exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i QuasselCore

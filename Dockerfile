FROM cloudron/base:0.3.0
MAINTAINER Johannes Zellner <johannes@nebulon.de>

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get install -y libldap2-dev build-essential libqt4-sql-sqlite qt4-default libqt4-dev

RUN mkdir -p /app
RUN cd /app && git clone https://github.com/abustany/quassel.git code

WORKDIR /app/code

RUN git checkout generic-ldap
RUN cmake /app/code -DWANT_CORE=ON -DWANT_QTCLIENT=OFF -DWANT_MONO=OFF -DWITH_KDE=OFF -DWITH_OXYGEN=OFF -DWITH_OPENSSL=ON -DWITH_WEBKIT=OFF -DWITH_LDAP=ON

RUN make
RUN make install

RUN npm install -g http-server

EXPOSE 4242
EXPOSE 8080

ADD welcome/ /app/code/welcome
ADD quasselcore.conf /app/code/quasselcore.conf
ADD quassel-storage.sqlite /app/code/quassel-storage.sqlite
ADD supervisor-quasselcore.conf /etc/supervisor/conf.d/quasselcore.conf
ADD supervisor-app.conf /etc/supervisor/conf.d/app.conf
ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
